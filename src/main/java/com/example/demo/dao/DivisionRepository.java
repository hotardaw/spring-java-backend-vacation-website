package com.example.demo.dao;

import com.example.demo.entities.Division;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("http://localhost:4200")
// Entity Division, Primary key Long:
public interface DivisionRepository extends JpaRepository<Division, Long> {
}
