// https://wgu.udemy.com/course/full-stack-angular-spring-boot-tutorial/learn/lecture/23548098#overview
package com.example.demo.services;

import lombok.Data;

@Data
public class PurchaseResponse {
    // Lombok @Data will only gen constructor for "final" fields
    // Must be final for the new PurchaseResponse return statement in CheckoutServiceImpl to accept a parameter
    private final String orderTrackingNumber;

    private String randomUUID;

    public void setRandomUUID(String randomUUID) {
        this.randomUUID = randomUUID;
    }
}
