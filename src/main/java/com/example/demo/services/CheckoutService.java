// https://wgu.udemy.com/course/full-stack-angular-spring-boot-tutorial/learn/lecture/23548104#overview
package com.example.demo.services;

public interface CheckoutService {

    PurchaseResponse placeOrder(Purchase purchase);

}
