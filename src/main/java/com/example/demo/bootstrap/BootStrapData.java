package com.example.demo.bootstrap;

import com.example.demo.dao.CustomerRepository;
import com.example.demo.dao.DivisionRepository;
import com.example.demo.entities.Customer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class BootStrapData implements CommandLineRunner {

    private final CustomerRepository customerRepository;
    private final DivisionRepository divisionRepository;

    public BootStrapData(CustomerRepository customerRepository, DivisionRepository divisionRepository) {
        this.customerRepository = customerRepository;
        this.divisionRepository = divisionRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        // Create a new Customer using the constructor
        Customer aaron = new Customer("111 Whatever Street", "Aaron", "Hotard", "1234567890", "11111", divisionRepository.getReferenceById(3L));
        saveCustomerIfNotExists(aaron);
        Customer eric = new Customer("222 Blah Blah Drive", "Eric", "White", "2345678901", "22222", divisionRepository.getReferenceById(6L));
        saveCustomerIfNotExists(eric);
        Customer ana = new Customer("333 Doesn't Matter Road", "Ana", "Thomas", "3456789012", "33333", divisionRepository.getReferenceById(60L));
        saveCustomerIfNotExists(ana);
        Customer davis = new Customer("444 Nonce Lane", "Davis", "Johnson", "4567890123", "44444", divisionRepository.getReferenceById(61L));
        saveCustomerIfNotExists(davis);
        Customer linda = new Customer("555 Fake Blvd", "Linda", "Carol", "5678901234", "55555", divisionRepository.getReferenceById(101L));
        saveCustomerIfNotExists(linda);


        // Retrieve & display all customers:
        Iterable<Customer> customers = customerRepository.findAll();
        for (Customer c : customers) {
            System.out.println(c);
        }
    }

    private void saveCustomerIfNotExists(Customer customer) {
        Optional<Customer> existingCustomer = customerRepository.findByPhone(customer.getPhone());
        if (existingCustomer.isEmpty()) {
            customerRepository.save(customer);
        }
    }

}
