// https://wgu.udemy.com/course/full-stack-angular-spring-boot-tutorial/learn/lecture/23548134#overview
package com.example.demo.controllers;

import com.example.demo.services.CheckoutService;
import com.example.demo.services.Purchase;
import com.example.demo.services.PurchaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/api/checkout")
public class CheckoutController {

    private CheckoutService checkoutService;

    @Autowired
    public CheckoutController(CheckoutService checkoutService) {
        this.checkoutService = checkoutService;
    }

    @PostMapping("/purchase")
    public PurchaseResponse placeOrder(@RequestBody Purchase purchase) {
       PurchaseResponse purchaseResponse = checkoutService.placeOrder(purchase);
        //return purchaseResponse;
        //return new PurchaseResponse(UUID.randomUUID().toString());
        purchaseResponse.setRandomUUID(UUID.randomUUID().toString()); // Set the random UUID
        return purchaseResponse;
    }



}
