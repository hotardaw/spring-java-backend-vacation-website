package com.example.demo.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "carts")
@Getter
@Setter
@ToString
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cart_id")
    private Long id; ////

    @Column(name = "package_price")
    private BigDecimal package_price; ////

    @Column(name = "party_size")
    private Integer party_size; ////

    @Column(name = "status", nullable = false) // maybe delete
    @Enumerated(EnumType.STRING)
    private StatusType status; ////

    @Column(name = "order_tracking_number")
    private String orderTrackingNumber; ////

    @Column(name = "create_date")
    @CreationTimestamp
    private LocalDateTime create_date;

    @Column(name = "last_update")
    @UpdateTimestamp
    private LocalDateTime last_update;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cart") // this is new
    private Set<CartItem> cartItems = new HashSet<>();

    public void add(CartItem cartItem) {
        cartItems.add(cartItem);
        cartItem.setCart(this);
    }
}