package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum StatusType {
    @JsonProperty("pending")
    PENDING("pending"),
    @JsonProperty("ordered")
    ORDERED("ordered"),
    @JsonProperty("canceled")
    CANCELED("canceled"); // In UML it's misspelled, in DB it's correct


    private final String value;

    StatusType(String value) {
        this.value = value;
    }
}
